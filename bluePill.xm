/* Copyright (C) alfaDesigns - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Suhaib Alfageeh <suhaib@alfadesigned.com>, 2014-2016
 */

#import "PropertyFile.h"
#import "substrate.h"

//extern "C" int xpc_connection_get_pid(id connection);

//
/****************************************************/
// File: Tweak-WIP.xm
// Objective - C and the LOGOS directive.
//  By: Suhaib Alfaqeeh
//  Copyright 2014-2016, alfaDesigns - All rights reserved.
// %hook className
// opens a hook block for an existing class
//Add a new method to a hooked class or subclass. signature is the Objective-C type encoding for the new method; if it is omitted, one will be generated.
//Can be inside a %group block.
// %orig, %orig(), and %orig(arg) executes logic without any hooks.
// If the method type is (void) you can follow %orig with your own logic as 'return' is never called.
// however if a method returns a type, any injected logic must come before calling %orig
/****************************************************/
// -(BOOL)methodName {	if (FActivate && FswitchName){ BOOL	r = FALSE; NSLog(@" = %d", r); return r; }
// else{ return %orig; } }
// -(void)methodName:(BOOL)arg1 {  if (FActivate && FswitchName){ %orig(FALSE); }
// else{ %orig(arg1); } }
/****************************************************/
/****************************************************/
// -(BOOL)methodName {	if (FActivate && FswitchName){ return TRUE; }
// else{ return %orig; } }
// -(void)methodName:(BOOL)arg1 {  if (FActivate && FswitchName){ %orig(TRUE); }
// else{ %orig(arg1); } }

/* %hook FBExperimentCatalog

- (void)setComponentsFeedNewsFeedMostRecentEnabled:(BOOL)fp8;
- (BOOL)componentsFeedNewsFeedMostRecentEnabled;
-fobjc-arc

 */
%group FBHooks

/****************************************************/
/****************************************************/
// Feature: FStealthMode
//     FStealthMode = [preferences[PREFERENCES_ENABLED_BPFStealthMode_KEY] boolValue];
/****************************************************/

/*****************Disable Delivery Receipts****************/
%hook ReadReceiptManager
-(BOOL)isDeliveryReceiptsEnabled {  if (FActivate && FStealthMode){ BOOL  r = FALSE; NSLog(@" StealthMode :  = %d", r); return r; }
else{ return %orig; } }

-(BOOL)isSendReceiptsEnabled {	if (FActivate && FStealthMode){ BOOL  r = FALSE; NSLog(@" StealthMode :  = %d", r); return r; }
else{ return %orig; } }
%end
/*****************Disable Typing Indicator****************/

%hook FBMMQTTSender
-(void)sendTypingNotification:(id)arg1 state:(int)arg2 {  if(FActivate && (FStealthMode || FNoTypingIndicator)){  %orig(NULL, 0); }
else { %orig(arg1, arg2); } }
/*****************Hide Online Status****************/
-(void)sendShowOnline:(BOOL)arg1 {  if (FActivate && FStealthMode){ %orig(FALSE); }
else{ %orig(arg1); }}

-(void)sendMarkThread:(id)arg1 completion:(id)arg2 {  if (FActivate && FStealthMode){ return; }
else{ %orig(arg1, arg2); }}

%end

%hook FBMessengerOnlineStatusManager
-(BOOL)started {  if (FActivate && FStealthMode){ BOOL	r = FALSE; NSLog(@" StealthMode :  = %d", r); return r; }
else{ return %orig; } }

-(BOOL)online {  if (FActivate && FStealthMode){ BOOL  r = FALSE; NSLog(@" StealthMode :  = %d", r); return r; }
else{ return %orig; } }

-(void)setOnline:(BOOL)arg1 {  if (FActivate && FStealthMode){ %orig(FALSE); }
else{ %orig(arg1); }}

-(BOOL)isOnline {  if (FActivate && FStealthMode){
BOOL  r = FALSE; NSLog(@" StealthMode : [FBMessengerModuleUserOnlineStatusManager isOnline] = %d False", r);
BOOL _online = MSHookIvar<int>(self,"_online"); _online = FALSE;
NSLog(@" StealthMode : *************  Setting _Online To: = %d False *************", _online);
return r; } else{ return %orig; } }

-(BOOL)isStarted {  if (FActivate && FStealthMode){
BOOL  r = FALSE; NSLog(@" StealthMode : [FBMessengerModuleUserOnlineStatusManager isStarted] = %d False", r);
BOOL _started = MSHookIvar<int>(self,"_started"); _started = FALSE;
NSLog(@" StealthMode : Online Mode:  Setting _started To: = %d False *************", _started);
return r; } else{ return %orig; } }

-(BOOL)getOnline {  if (FActivate && FStealthMode){ BOOL  r = FALSE; NSLog(@" StealthMode :  = %d", r); return r; }
else{ return %orig; } }


%end

//==================
%hook FBSyncPresence
-(unsigned int)presenceState {
if(FStealthMode){
return 0; }
else{ return %orig; } }
%end

%hook FBMQTTClientManager
-(void)configureWithClientID:(id)arg1 showOnline:(BOOL)arg2 endpointCapabilities:(int)arg3 credentialsAvailableBlock:(id)arg4 credentialsFetcherBlock:(id)arg5 userAgentFormatterBlock:(id)arg6 {
%log;
if (FActivate && FStealthMode) {
%orig(arg1, FALSE, arg3, arg4, arg5, arg6);
}
else{
%orig(arg1, arg2, arg3, arg4, arg5, arg6);
}}

-(void)setReachable:(BOOL)arg1 {
if(FStealthMode){
%orig(FALSE); }
else{ %orig(arg1); } }

-(BOOL)reachable {  if (FActivate && FStealthMode){ BOOL  r = FALSE; NSLog(@" StealthMode :  = %d", r); return r; }
else{ return %orig; } }
%end

%hook FBMMQTTManager

-(void)setOnlineStatusManager:(id)arg1 {
if(FStealthMode){
return; }
else{ %orig(arg1); } }

-(BOOL)isConnected {  if (FActivate && FStealthMode){ BOOL  r = FALSE; NSLog(@" StealthMode :  = %d", r); return r; }
else{ return %orig; } }
%end


%hook FBSyncStore

-(BOOL)shouldSyncLastActiveOnPresenceSync
 {  if (FActivate && FStealthMode){	BOOL	r = FALSE; NSLog(@" StealthMode :  = %d", r); return r; }
else{ return %orig; } }

-(void)setShouldSyncLastActiveOnPresenceSync:(BOOL)arg1 {
if(FStealthMode){
%orig(FALSE); }
else{ %orig(arg1); } }

%end

/*****************Hide Active Status****************/
%hook FBLastActiveTimeReader
-(BOOL)wasLastActiveTimeLessThanIntervalFromCurrentTime:(double)arg1 {	if (FActivate && FStealthMode){ BOOL  r = FALSE; NSLog(@" StealthMode :  = %d", r); return r; }
else{ return %orig; } }
%end

%hook FBMessengerUser
-(int)onlinePresence {	if(FStealthMode){ return 0; } else{ return %orig; }}

-(BOOL)isActive {  if (FActivate && FStealthMode){ BOOL  r = FALSE; NSLog(@" StealthMode :  = %d", r); return r; }
else{ return %orig; } }

-(void)setIsActive:(BOOL)arg1 {
if(FStealthMode){
%orig(FALSE); }
else{ %orig(arg1); } }

-(void)setLastActiveTime:(long long)arg1 {  if(FStealthMode){
long long r = 000000000; NSLog(@" StealthMode : *************	Setting Last Actime Time To: = %lld *************", r); %orig(r); }
else{ %orig(arg1); } }

%end //end FBMessengerUser
/*****************Disable Read Receipts****************/
%hook MessagesViewController
-(void)maybeMarkThreadRead {
%log;
if (FActivate && (FStealthMode || FNoReadReciepts)){
//do nothing
return; }
else{ %orig; }}
%end
////////////////////////////////////////////////////////////////
 %hook FBNavigationViewCoordinatorConfig
  /*
  -(BOOL)allowUserComposingOutsideComposer {  if (FActivate && FExperiments){ return TRUE; }
else{ return %orig; } }
-(void)setAllowUserComposingOutsideComposer:(BOOL)arg1 {  if (FActivate && FExperiments){ %orig(TRUE); }
else{ %orig(arg1); } } */

/****************************************************/
// Feature: FHideTabLabels
//     FHideTabLabels = [preferences[PREFERENCES_ENABLED_BPFHideTabLabels_KEY] boolValue];
/****************************************************/
-(BOOL)tabBarShowsTitles {  if (FActivate && FHideTabLabels){ BOOL  r = FALSE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
-(void)setTabBarShowsTitles:(BOOL)arg1 {  if (FActivate && FHideTabLabels){ %orig(TRUE); }
else{ %orig(arg1); } }
 /****************************************************/
// Feature: FSideNavBar
//     FSideNavBar = [preferences[PREFERENCES_ENABLED_BPFSideNavBar_KEY] boolValue];
/****************************************************/
-(BOOL)hasDiveBar {  if (FActivate && FSideNavBar){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
-(void)setHasDiveBar:(BOOL)arg1 {  if (FActivate && FSideNavBar){ %orig(TRUE); }
else{ %orig(arg1); } }
/****************************************************/
// Feature: Experiments
//  FExperiments = [preferences[PREFERENCES_ENABLED_BPFExperiments_KEY] boolValue];
/****************************************************/
-(BOOL)enableBookmarkListAppSection {  if (FActivate && FExperiments){ BOOL	r = FALSE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
-(void)setEnableBookmarkListAppSection:(BOOL)arg1 {  if (FActivate && FExperiments){ %orig(FALSE); }
else{ %orig(arg1); } }

 -(BOOL)jewelsHidden {	if (FActivate && FTopOptions){ BOOL	r = FALSE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
-(void)setJewelsHidden:(BOOL)arg1 {  if (FActivate && FTopOptions){ %orig(FALSE); }
else{ %orig(arg1); } }

 -(BOOL)enableTopNavMoreOptionsButton {  if (FActivate && FTopOptions){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
-(void)setEnableTopNavMoreOptionsButton:(BOOL)arg1 {  if (FActivate && FTopOptions){ %orig(TRUE); }
else{ %orig(arg1); } }

/* -(BOOL)enableTimelineAppSection {  if (FActivate && FExperiments){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
-(void)setEnableTimelineAppSection:(BOOL)arg1 {  if (FActivate && FExperiments){ %orig(TRUE); }
else{ %orig(arg1); } } */


/* -(BOOL)enableComposerAppSection {  if (FActivate && FExperiments){ BOOL  r = FALSE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
-(void)setEnableComposerAppSection:(BOOL)arg1 {  if (FActivate && FExperiments){ %orig(FALSE); }
else{ %orig(arg1); } } */

 -(BOOL)hasWhiteSidebar {  if (FActivate && FSideNavBar){ BOOL	r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
-(void)setHasWhiteSidebar:(BOOL)arg1 {	if (FActivate && FSideNavBar){ %orig(TRUE); }
else{ %orig(arg1); } }

/* -(BOOL)enableMentionsAppSection {  if (FActivate && FExperiments){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
-(void)setEnableMentionsAppSection:(BOOL)arg1 {  if (FActivate && FExperiments){ %orig(TRUE); }
else{ %orig(arg1); } } */

 -(BOOL)usesGridLauncher {  if (FActivate && FSideNavBar){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
-(void)setUsesGridLauncher:(BOOL)arg1 {  if (FActivate && FSideNavBar){ %orig(TRUE); }
else{ %orig(arg1); } }


/* -(BOOL)useFIGStyleCellsInTabs {  if (FActivate && FExperiments){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } } */

 -(BOOL)hasSideNav {  if (FActivate && FSideNavBar){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
-(void)setHasSideNav:(BOOL)arg1 {  if (FActivate && FSideNavBar){ %orig(TRUE); }
else{ %orig(arg1); } }
 %end


 %hook FBFeedComponentsExperimentContext

 -(BOOL)componentsEnabled {  if (FActivate && FExperiments){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }

 -(BOOL)ufiEnableEverywhere {  if (FActivate && FExperiments){ BOOL	r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }

 -(BOOL)componentsFeedNewsFeedMostRecentEnabled {  if (FActivate && FExperiments){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }

-(void)setComponentsFeedNewsFeedMostRecentEnabled:(BOOL)arg1 {	if (FActivate && FExperiments){ %orig(TRUE); }
else{ %orig(arg1); } }
 %end
 //=====================================================
 %hook FBAppSectionManager
-(BOOL)canChangeSelectedAppSectionToSection:(int)arg1 {  if (FActivate && FExperiments){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig(arg1); } }
 %end

%hook FBBookmarkExperimentConfiguration
+(BOOL)allowUserInterfaceIdiom:(int)arg1 { if (FActivate && FExperiments){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig(arg1); } }

-(void)setExperimentEnabled:(BOOL)arg1 {  if (FActivate && FExperiments){ %orig(TRUE); }
else{ %orig(arg1); } }

-(BOOL)experimentEnabled {if (FActivate && FExperiments){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }

-(BOOL)useNewSections { if (FActivate && FExperiments){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }

-(BOOL)downloadFromGraphQL { if (FActivate && FExperiments){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }

-(BOOL)includeRelevantNowSection { if (FActivate && FExperiments){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }

/* -(BOOL)shouldCollapseHeaders {  if (FActivate && FExperiments){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } } */
%end


/****************************************************/
// Feature: No Messenger
// FNoMessenger = [preferences[PREFERENCES_ENABLED_BPFNoMessenger_KEY] boolValue];
/****************************************************/
%hook FBMessengerUser
-(BOOL)canInstallMessenger {  if (FActivate && FNoMessenger){ return FALSE;	}
else{ return %orig; } }

-(BOOL)hasMessenger {  if (FActivate && FNoMessenger){
return TRUE;
}
else{ return %orig; } }
%end

%hook FBMessengerInterstitialController
/* -(BOOL)_shouldRedirectToMessengerNeue {  if (FActivate && FNoMessenger){ return FALSE;	}
else{ return %orig; } } */


-(BOOL)_shouldShowPromoNowWithRefreshInterval:(int)arg1 {  %log; if (FActivate && FNoMessenger){ return %orig(FALSE);  }
else{ return %orig; } }
%end

%hook FBMessengerModuleAppProperties
-(BOOL)promotionEnabled {  if (FActivate && FNoMessenger){ return FALSE;  }
else{ return %orig; } }
%end
/****************************************************/
// Feature: Schedule Posts (Experimental)
//  FSchedulePosts = [preferences[PREFERENCES_ENABLED_BPFSchedulePosts_KEY] boolValue];
/****************************************************/

/* %hook FBComposerTraits
-(BOOL)allowPhotoDragging {   if (FActivate && FSchedulePosts){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; }}
%end

%hook FBComposerTraitsBuilder

-(id)withAllowPostOptions:(BOOL)arg1 {	if (FActivate && FSchedulePosts){ return %orig(TRUE); }
else{ return %orig(arg1); }}

%end
 */

/****************************************************/
// Feature: FDisableVOIP
//  FSchedulePosts = [preferences[PREFERENCES_ENABLED_BPFSchedulePosts_KEY] boolValue];
/****************************************************/
%hook FBMQTTClient

-(BOOL)useVOIP {  if (FActivate && FDisableVOIP){ return FALSE;  }
else{ return %orig; }}
%end

%hook FBMessengerModuleAppProperties
-(BOOL)supportVOIP {  if (FActivate && FDisableVOIP){ %orig; return FALSE;  }
else{ return %orig; } }
%end

%hook VOIPConfiguration
-(BOOL)enableVOIP {  if (FActivate && FDisableVOIP){ return FALSE;  }
else{ return %orig; } }

-(void)setEnableVOIP:(BOOL)arg1 {  if (FActivate && FSchedulePosts){ %orig(FALSE); }
else{ %orig(arg1); }}
%end
/****************************************************/
// Feature: FGraphSearch (experimental)
//  FGraphSearch = [preferences[PREFERENCES_ENABLED_BPFGraphSearch_KEY] boolValue];
/****************************************************/

%hook FBTimelinePrefetchHeaderInfoOnNonTimelineSurfacesExperimentContext
-(BOOL)isEnabledForGraphSearch {  if (FActivate && FGraphSearch){ BOOL	r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
%end

 %hook FBUserSession
 -(BOOL)discoveryEnabled {  if (FActivate && FGraphSearch){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
-(BOOL)graphSearchEnabled {  if (FActivate && FGraphSearch){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
%end

%hook FBGraphSearchConfig
-(BOOL)isEnabled {  if (FActivate && FGraphSearch){ BOOL  r = TRUE; NSLog(@" = %d", r); return r; }
else{ return %orig; } }
%end
/****************************************************/
// Feature: FSendUnlimPhotos
//   FSendUnlimPhotos = [preferences[PREFERENCES_ENABLED_BPFSendUnlimPhotos_KEY] boolValue];
/****************************************************/
%hook FBMediaAttachmentPickerControllerConfiguration
-(unsigned int)maxPhotosSelected {  if (FActivate && FSendUnlimPhotos){
unsigned int r = 9999999;
NSLog(@" = %d", r);
return r;
}
else{
unsigned int r = %orig;
NSLog(@" = %u", r);
return r;
}
}
%end
/****************************************************/
// Feature: FFullscreenMode
//    FFullscreenMode = [preferences[PREFERENCES_ENABLED_BPFFullscreenMode_KEY] boolValue];
/****************************************************/
#pragma mark - UIStatusBar
%hook UIApplication
-(BOOL) isStatusBarHidden {
    if (FActivate && FFullscreenMode) {
	return YES;
	} else {
		return %orig;
	}
}

-(void)removeStatusBarItem:(int)ar1{

	if (FActivate && FFullscreenMode) {
	return;
	} else {
		return %orig;
	}
}

-(void)removeStatusBarStyleOverrides:(int)arg1{
    if (FActivate && FFullscreenMode) {
	return;
	} else {
		return %orig;
	}
}

-(id)statusBar{
    if (FActivate && FFullscreenMode) {
	return nil;
	} else {
		return %orig;
	}
}

%end
%hook UIStatusBar
-(BOOL)isHidden  {
    if (FActivate && FFullscreenMode) {
	return YES;
	} else {
		return %orig;
	}
}

-(void)setHidden:(BOOL)arg1{
    if (FActivate && FFullscreenMode) {
	return;
	} else {
		return %orig;
	}
}

-(int)styleForRequestedStyle:(int)arg1{
    if (FActivate && FFullscreenMode) {
	return 0;
	} else {
		return %orig;
	}
}

- (id)initWithFrame:(id)arg1 {
    if (FActivate && FFullscreenMode) {
	return nil;
	} else {
		return %orig;
	}
}
- (int)_foregroundAlphaForStatusBarStyle {
    if (FActivate && FFullscreenMode) {
	return 0;
    } else {
	return %orig;
    }
}
- (int)_foregroundStyleForStatusBarStyle {
    if (FActivate && FFullscreenMode) {
	return 0;
    } else {
	return %orig;
    }
}
%end
%hook UIStatusBarStyleRequest
- (id)initWithStyle:(int)arg1 legacy:(BOOL)arg2 legibilityStyle:(int)arg3 foregroundColor:(id)arg4 {
    if (FActivate && FFullscreenMode) {
	return nil;
	} else {
		return %orig;
	}
}
%end

%hook UIStatusBarNewUIForegroundStyleAttributes
- (id)initWithHeight:(float)arg1 legibilityStyle:(int)arg2 tintColor:(id)arg3 backgroundColor:(id)arg4 {
    if (FActivate && FFullscreenMode) {
	arg3 = [UIColor clearColor];
	return %orig(arg1, arg2, arg3, arg4);
    } else {
	return %orig;
    }
}
%end
/****************************************************/
// Feature: Employee Settings
//     FEmployeeSettings = [preferences[PREFERENCES_ENABLED_BPEmployeeSettings_KEY] boolValue];

/****************************************************/
%hook FBLoggedOutExperimentManagerPolicy
-(BOOL)isEmployee { if (FActivate && FEmployeeSettings){ NSLog(@"Internal Settings Activated"); BOOL r = TRUE; return r; }
else{  NSLog(@"Internal Settings Deactivated"); return %orig; } }
%end

%hook FBSessionExperimentManagerPolicy
-(BOOL)isEmployee { if (FActivate && FEmployeeSettings){ NSLog(@"Internal Settings Activated"); BOOL r = TRUE; return r; }
else{  NSLog(@"Internal Settings Deactivated"); return %orig; } }
%end

%hook FBUserPreferences
//override method 3.1 isEmployeeValue
-(BOOL)isEmployee { if (FActivate && FEmployeeSettings){ NSLog(@"Internal Settings Activated"); BOOL r = TRUE; return r; }
else{  NSLog(@"Internal Settings Deactivated"); return %orig; } }
%end

%hook FBBugReportShakeHandler
-(BOOL)isEmployee { if (FActivate && FEmployeeSettings){ NSLog(@"Internal Settings Activated"); BOOL r = TRUE; return r; }
else{  NSLog(@"Internal Settings Deactivated"); return %orig; } }
%end

%hook FBBugReportInitialCoordinator
-(BOOL)isEmployee { if (FActivate && FEmployeeSettings){ NSLog(@"Internal Settings Activated"); BOOL r = TRUE; return r; }
else{  NSLog(@"Internal Settings Deactivated"); return %orig; } }
%end

%hook FBBugReportContainerViewController
-(BOOL)isEmployee { if (FActivate && FEmployeeSettings){ NSLog(@"Internal Settings Activated"); BOOL r = TRUE; return r; }
else{  NSLog(@"Internal Settings Deactivated"); return %orig; } }
%end

%hook FBBugReportChooseFeatureViewController
-(BOOL)isEmployee { if (FActivate && FEmployeeSettings){ NSLog(@"Internal Settings Activated"); BOOL r = TRUE; return r; }
else{  NSLog(@"Internal Settings Deactivated"); return %orig; } }
%end

%hook FBExceptionHandler
-(BOOL)isEmployee { if (FActivate && FEmployeeSettings){ NSLog(@"Internal Settings Activated"); BOOL r = TRUE; return r; }
else{  NSLog(@"Internal Settings Deactivated"); return %orig; } }

-(void)setUserIsEmployee:(BOOL)arg1 { if (FActivate && FEmployeeSettings){ NSLog(@"Internal Settings Activated"); BOOL r = TRUE; %orig(r); }
else{  NSLog(@"Internal Settings Deactivated"); return %orig; } }
%end
/****************************************************/
// Feature: FTimestampsPlus
//     FTimestampsPlus = [preferences[PREFERENCES_ENABLED_BPFTimestampsPlus_KEY] boolValue];
/****************************************************/
%hook ThreadMessageRow
-(BOOL)shouldShowTime {  if (FActivate && FTimestampsPlus){ BOOL r = TRUE; NSLog(@"Setting -(BOOL)shouldShowTime to False | Bool = %d *******", r); return r; }
else{ return %orig; } }
%end
/****************************************************/
// Feature: FStopAutoplay
//     FStopAutoplay = [preferences[PREFERENCES_ENABLED_BPFStopAutoplay_KEY] boolValue];
/****************************************************/
%hook FBFeedCell
-(BOOL)shouldAutoplayInlineVideo {  if (FActivate && FStopAutoplay){ BOOL r = FALSE; NSLog(@"Setting -(BOOL)shouldAutoplayInlineVideo to False | Bool = %d *******", r); return r; }
else{ BOOL r = %orig; return r; } }
%end

%hook FBFeedScrollingVideoController
-(void)resumeInlineVideos {  if(FStopAutoplay){ NSLog(@"Setting -(void)resumeInlineVideos to do NOTHING"); }
else{ %orig; }}
%end

%hook FBVideoPlayerView
-(BOOL)showsAudioMeters {  if (FActivate && FStopAutoplay){ BOOL r = FALSE; NSLog(@"Setting -(BOOL)showsAudioMeters to False | Bool = %d *******", r); return r; }
else{ BOOL r = %orig; return r; } }

-(BOOL)shouldAutoplayInFeed {  if (FActivate && FStopAutoplay){ BOOL r = FALSE; NSLog(@"Setting -(BOOL)shouldAutoplayInFeed to False | Bool = %d *******", r); return r; }
else{ return %orig; } }

-(void)setMute:(BOOL)r {  if(FStopAutoplay){ r = FALSE; NSLog(@"Setting -(void)setMute:(FALSE) | Bool = %d *******", r); %orig(r); }
else{ %orig(r); } }

-(BOOL)autoplayInFeed {  if (FActivate && FStopAutoplay){ BOOL r = FALSE; NSLog(@"Setting -(BOOL)autoplayInFeed to False | Bool = %d *******", r); return r; }
else{ BOOL r = %orig; return r; } }
%end

%end //END HOOK GROUP
/****************************************************/
static void PreferencesChangedCallback(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo) {

    system("killall -9 Facebook");
    system("killall -9 Settings");

    NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH];

	 FActivate = [preferences[PREFERENCES_ENABLED_BPFActivate_KEY] boolValue];

 FExperiments = [preferences[PREFERENCES_ENABLED_BPFExperiments_KEY] boolValue];
 FSideNavBar = [preferences[PREFERENCES_ENABLED_BPFSideNavBar_KEY] boolValue];
 FMostRecent = [preferences[PREFERENCES_ENABLED_BPFMostRecent_KEY] boolValue];
 FNoMessenger = [preferences[PREFERENCES_ENABLED_BPFNoMessenger_KEY] boolValue];
 FSchedulePosts = [preferences[PREFERENCES_ENABLED_BPFSchedulePosts_KEY] boolValue];
 FDisableVOIP = [preferences[PREFERENCES_ENABLED_BPFDisableVOIP_KEY] boolValue];
 FGraphSearch = [preferences[PREFERENCES_ENABLED_BPFGraphSearch_KEY] boolValue];
 FSendUnlimPhotos = [preferences[PREFERENCES_ENABLED_BPFSendUnlimPhotos_KEY] boolValue];
 FFullscreenMode = [preferences[PREFERENCES_ENABLED_BPFFullscreenMode_KEY] boolValue];
 FEmployeeSettings = [preferences[PREFERENCES_ENABLED_BPFEmployeeSettings_KEY] boolValue];
 FStealthMode = [preferences[PREFERENCES_ENABLED_BPFStealthMode_KEY] boolValue];
 FNoReadReciepts = [preferences[PREFERENCES_ENABLED_BPFNoReadReciepts_KEY] boolValue];
 FNoTypingIndicator = [preferences[PREFERENCES_ENABLED_BPFNoTypingIndicator_KEY] boolValue];
 FTimestampsPlus = [preferences[PREFERENCES_ENABLED_BPFTimestampsPlus_KEY] boolValue];
 FHideTabLabels = [preferences[PREFERENCES_ENABLED_BPFHideTabLabels_KEY] boolValue];
 FStopAutoplay = [preferences[PREFERENCES_ENABLED_BPFStopAutoplay_KEY] boolValue];
 FHDAlways = [preferences[PREFERENCES_ENABLED_BPFHDAlways_KEY] boolValue];

}

%ctor {



    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *block) {


	NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH];

	if (preferences == nil) {
	    preferences = @{
				    PREFERENCES_ENABLED_BPFActivate_KEY : @(NO),

		    PREFERENCES_ENABLED_BPFExperiments_KEY : @(NO),
			PREFERENCES_ENABLED_BPFSideNavBar_KEY : @(NO),
			PREFERENCES_ENABLED_BPFTopOptions_KEY : @(NO),
			PREFERENCES_ENABLED_BPFNoMessenger_KEY : @(NO),
			PREFERENCES_ENABLED_BPFMostRecent_KEY : @(NO),
			PREFERENCES_ENABLED_BPFSchedulePosts_KEY : @(NO),
			PREFERENCES_ENABLED_BPFDisableVOIP_KEY : @(NO),
			PREFERENCES_ENABLED_BPFGraphSearch_KEY : @(NO),
			PREFERENCES_ENABLED_BPFSendUnlimPhotos_KEY : @(NO),
			PREFERENCES_ENABLED_BPFFullscreenMode_KEY : @(NO),
			PREFERENCES_ENABLED_BPFEmployeeSettings_KEY : @(NO),
			PREFERENCES_ENABLED_BPFStealthMode_KEY : @(NO),
			PREFERENCES_ENABLED_BPFNoReadReciepts_KEY : @(NO),
			PREFERENCES_ENABLED_BPFNoTypingIndicator_KEY : @(NO),
			PREFERENCES_ENABLED_BPFTimestampsPlus_KEY : @(NO),
			PREFERENCES_ENABLED_BPFHideTabLabels_KEY : @(NO),
			PREFERENCES_ENABLED_BPFStopAutoplay_KEY : @(NO),
			PREFERENCES_ENABLED_BPFHDAlways_KEY : @(NO)};

	    [preferences writeToFile:PREFERENCES_PATH atomically:YES];
	} else {

FActivate = YES;

 FExperiments = YES;
 FSideNavBar = YES;
 FTopOptions = YES;
 FNoMessenger = YES;
 FMostRecent = YES;
 FSchedulePosts = YES;
 FDisableVOIP = YES;
 FGraphSearch = YES;
 FSendUnlimPhotos = YES;
 FFullscreenMode = YES;
 FEmployeeSettings = YES;
 FStealthMode = YES;
 FNoReadReciepts = YES;
 FNoTypingIndicator = YES;
 FTimestampsPlus = YES;
 FHideTabLabels = YES;
 FStopAutoplay = YES;
 FHDAlways = YES;

 FActivate = [preferences[PREFERENCES_ENABLED_BPFActivate_KEY] boolValue];

 FExperiments = [preferences[PREFERENCES_ENABLED_BPFExperiments_KEY] boolValue];
 FSideNavBar = [preferences[PREFERENCES_ENABLED_BPFSideNavBar_KEY] boolValue];
 FTopOptions = [preferences[PREFERENCES_ENABLED_BPFTopOptions_KEY] boolValue];
 FNoMessenger = [preferences[PREFERENCES_ENABLED_BPFNoMessenger_KEY] boolValue];
 FMostRecent = [preferences[PREFERENCES_ENABLED_BPFMostRecent_KEY] boolValue];
 FSchedulePosts = [preferences[PREFERENCES_ENABLED_BPFSchedulePosts_KEY] boolValue];
 FDisableVOIP = [preferences[PREFERENCES_ENABLED_BPFDisableVOIP_KEY] boolValue];
 FGraphSearch = [preferences[PREFERENCES_ENABLED_BPFGraphSearch_KEY] boolValue];
 FSendUnlimPhotos = [preferences[PREFERENCES_ENABLED_BPFSendUnlimPhotos_KEY] boolValue];
 FFullscreenMode = [preferences[PREFERENCES_ENABLED_BPFFullscreenMode_KEY] boolValue];
 FEmployeeSettings = [preferences[PREFERENCES_ENABLED_BPFEmployeeSettings_KEY] boolValue];
 FStealthMode = [preferences[PREFERENCES_ENABLED_BPFStealthMode_KEY] boolValue];
 FNoReadReciepts = [preferences[PREFERENCES_ENABLED_BPFNoReadReciepts_KEY] boolValue];
 FNoTypingIndicator = [preferences[PREFERENCES_ENABLED_BPFNoTypingIndicator_KEY] boolValue];
 FTimestampsPlus = [preferences[PREFERENCES_ENABLED_BPFTimestampsPlus_KEY] boolValue];
 FHideTabLabels = [preferences[PREFERENCES_ENABLED_BPFHideTabLabels_KEY] boolValue];
 FStopAutoplay = [preferences[PREFERENCES_ENABLED_BPFStopAutoplay_KEY] boolValue];
 FHDAlways = [preferences[PREFERENCES_ENABLED_BPFHDAlways_KEY] boolValue];

			if(FActivate){
	NSLog(@"bluePill: bluePill Activated: Initializing");
	%init(FBHooks);
	}
	else{
	NSLog(@"bluePill: bluePill Deactivated. Target will run unmodified.");
	}

	}
	CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, PreferencesChangedCallback, CFSTR(PREFERENCES_CHANGED_NOTIFICATION), NULL, CFNotificationSuspensionBehaviorCoalesce);
    }];
}
