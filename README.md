![](http://s10.postimg.org/so05ug5j9/1763171714_new_Header_Icon_2x.png)
## bluePill - Source Code Sample
-----------------------------

bluePill: Enhances the functionality of Facebook social media applications from the Apple Appstore by modifying how the apps work. 

###The Idea: 
- Look at the app as one giant switchboard. 
- Observe what happens when you turn some switches on, some off.
- Create a feature set based on the results. (Settings pane in Settings.app will allow users to switch on/off

Features included for the following apps:
- Facebook.app
- Messenger.app
- Paper.app

### A product of reverse engineering on iOS By: Suhaib Alfageeh
### Language: Objective-C Logos preprocessor directives. 

- 1) Download Facebook 
- 2) Dump the binary 
- 3) Analyze assembly 
- 4) Study code-base from dumped binary headers included in Facebook binary 
- 5) Inject own logic into hooked classes.
